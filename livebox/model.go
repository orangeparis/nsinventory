package model

// LiveboxFeatures describing  a livebox test slot ( line type , line speed, livebox type)
type LiveboxFeatures struct {
	Name        string // a unique name identifing a set of features ( or the allias )
	LineType    string // ADSL, FTH ...
	LiveboxType string //  LIVEBOX3 , MIB4 ...
	LineSpeed   string // line speed  eg 20MB
}

// LiveboxSLot : a slot for a livebox
type LiveboxSLot struct {
	Name     string           // name of the slot ( or the allias )
	Location string           // describe the physical location site/room/rack/cell
	Features *LiveboxFeatures // the purpose features

	LineAccount string // eg fti/abcdef

	// data updated by platform design
	PluggedInLivebox    string // name of the LiveboxPlug
	AssignedPowerOutlet string // name of the outlet eg PW1_1

}

// LiveboxPlug  properties of the livebox plugged in LiveboxSlot
type LiveboxPlug struct {
	Name string // name id of the livebox

	//TelnetUserRoot         string
	//TelnetUserRootPassword string

	// data to be updated by live ip watch
	LiveboxType  string // "Livebox 3", ...
	SerialNumber string
	Mac          string
	Ipv4         string
}

// Powerswitch a ip manageable powerswitch
type Powerswitch struct {
	Name  string
	Model string // eg Powerswitch

	IPV4 string
	Mac  string

	NbOutlets int
}

// PowerOutlet the powerswitch outlet
type PowerOutlet struct {
	Name string // name + outlet eg PW1_1

	Powerswitch string // powerswitch name eg PW1

	Outlet int // the outlet number ( 1 , 2  ...)
}
